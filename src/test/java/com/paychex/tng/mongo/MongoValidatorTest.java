package com.paychex.tng.mongo;

import com.mongodb.MongoClient;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoCursor;
import com.mongodb.client.MongoDatabase;
//import com.paychex.tng.mongo.ValidationRepository;

import static org.junit.Assert.*;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;

import uk.co.datumedge.hamcrest.json.SameJSONAs;

import org.bson.Document;
import org.json.*;

import org.slf4j.LoggerFactory;
import ch.qos.logback.classic.Logger;

import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.boot.test.context.SpringBootTest;

@RunWith(SpringJUnit4ClassRunner.class)
//@SpringBootTest
public class MongoValidatorTest {
	static Logger log = (Logger)LoggerFactory.getLogger(MongoValidatorTest.class);
	
//	@Autowired
//	private ValidationRepository myRepository;
	
	/**
	 * Prerequisites:
	 * 1) MongoD is running on the local machine
	 * 2) The Config Validator has been run (getTestJason.bat)
	 * 
	 * I:\TNG Report Compare\configValidator
	 * 
	 * NOTE: This test is not considered valid as it does not pull all data from the DB, only what is defined in the repository class.
	 */
//	@Test
//	public final void testMongoSpring() throws FileNotFoundException, IOException {
//		JSONObject jsonExpected = scanJsonFile("src/test/resources/configFile_expected_old.json");
//		JSONObject jsonGiven = new JSONObject( myRepository.findAll().get(0).toString().replace("\\", "\\\\") );
//		
//		//http://stackoverflow.com/questions/2253750/compare-two-json-objects-in-java
//		assertEquals("JSON found to be null [jsonExpected]", true, jsonExpected != null);
//		assertEquals("JSON found to be null [jsonGiven]", true, jsonGiven != null);
//		Assert.assertThat(jsonGiven, SameJSONAs.sameJSONObjectAs(jsonExpected).allowingAnyArrayOrdering());
//	}
	
	/**
	 * Prerequisites:
	 * 1) MongoD is running on the local machine
	 * 2) The Config Validator has been run (getTestJason.bat)
	 * 
	 * I:\TNG Report Compare\configValidator
	 * 
	 * @throws FileNotFoundException
	 * @throws IOException
	 */
	@Test
	public final void testMongoDirect() throws FileNotFoundException, IOException {
		String host = "localhost";
		int port = 27017;
		String database = "TNG_PDF_EXTRACTION_DATA";
		String collection = "validation";
		
		JSONObject jsonExpected = scanJsonFile("src/test/resources/configFile_expected.json");
		JSONObject jsonGiven = scanMongoDB(host, port, database, collection);
		
//		//http://stackoverflow.com/questions/2253750/compare-two-json-objects-in-java
		assertEquals("JSON found to be null [jsonExpected]", true, jsonExpected != null);
		assertEquals("JSON found to be null [jsonGiven]", true, jsonGiven != null);
		Assert.assertThat(jsonGiven, SameJSONAs.sameJSONObjectAs(jsonExpected).allowingAnyArrayOrdering());
	}
	
	/**
	 * 
	 * @param host
	 * @param port
	 * @param database
	 * @param collection
	 * @return
	 */
	private JSONObject scanMongoDB(String host, int port, String database, String collection) {
		MongoClient mongoClient = new MongoClient(host, port);
		MongoDatabase db = mongoClient.getDatabase(database);
		MongoCollection<Document> dbCollection = db.getCollection(collection);
		MongoCursor<Document> iter = dbCollection.find().iterator();
		
		String output = "";
		
		try {
			while(iter.hasNext()) {
				Document doc = iter.next();
				output = output + doc.toJson();
				
				if (iter.hasNext()) {
					output = output + ",";
				}
			}
		} finally {
			iter.close();
			mongoClient.close();
		}
		
		return new JSONObject(output);
	}
	
	/**
	 * Scans a given JSON file into a usable JSONObject
	 * @param filename [String] Name of the JSON file to scan.
	 * @return A comparable JSONObject representation of the passed in JSON file.
	 * @throws IOException 
	 */
	private JSONObject scanJsonFile(String filename) throws IOException {
		JSONObject retVal = null;
		
		// Reference: http://javarevisited.blogspot.com/2015/09/how-to-read-file-into-string-in-java-7.html
		retVal = new JSONObject(new String(Files.readAllBytes(Paths.get(filename))));
		
		return retVal;
	}
}
