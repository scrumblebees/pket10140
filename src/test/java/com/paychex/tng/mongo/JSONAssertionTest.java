package com.paychex.tng.mongo;

import static org.hamcrest.CoreMatchers.not;
import static org.junit.Assert.*;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;

import uk.co.datumedge.hamcrest.json.SameJSONAs;
import org.json.*;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import org.slf4j.LoggerFactory;
import ch.qos.logback.classic.Logger;

import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
public class JSONAssertionTest {
	static Logger log = (Logger)LoggerFactory.getLogger(JSONAssertionTest.class);

	@Test
	public final void testEqualUnorderedArrayStrings_Jackson() throws IOException {
		String json1 = "{\"val1\":\"value!\",\"val2\":[\"one\",\"two\"]}";
		String json2 = "{\"val1\":\"value!\",\"val2\":[\"two\",\"one\"]}";
		
		ObjectMapper mapper = new ObjectMapper();
		JsonNode jsonNode1 = mapper.readTree(json1.getBytes());
		JsonNode jsonNode2 = mapper.readTree(json2.getBytes());

		assertEquals("Equal 'unordered array' string-based JSON found not to match (ideally, this should pass). "+
				"JSON1 = " +json1+ ", JSON2 = " +json2, true, jsonNode1.equals(jsonNode2));
	}
	
	@Test
	public final void testEqualUnorderedObjectStrings_Jackson() throws IOException {
		String json1 = "{\"val2\":[\"one\",\"two\"],\"val1\":\"value!\"}";
		String json2 = "{\"val1\":\"value!\",\"val2\":[\"two\",\"one\"]}";
		
		ObjectMapper mapper = new ObjectMapper();
		JsonNode jsonNode1 = mapper.readTree(json1.getBytes());
		JsonNode jsonNode2 = mapper.readTree(json2.getBytes());

		assertEquals("Equal 'unordered object' string-based JSON found not to match (ideally, this should pass). "+
				"JSON1 = " +json1+ ", JSON2 = " +json2, true, jsonNode1.equals(jsonNode2));
	}
	
	@Test
	public final void testEqualOrderedStrings_Jackson() throws IOException {
		String json1 = "{\"val1\":\"value!\",\"val2\":[\"one\",\"two\"]}";
		String json2 = "{\"val1\":\"value!\",\"val2\":[\"one\",\"two\"]}";
		
		ObjectMapper mapper = new ObjectMapper();
		JsonNode jsonNode1 = mapper.readTree(json1.getBytes());
		JsonNode jsonNode2 = mapper.readTree(json2.getBytes());

		assertEquals("Equal ordered string-based JSON found not to match. "+
				"JSON1 = " +json1+ ", JSON2 = " +json2, true, jsonNode1.equals(jsonNode2));
	}
	
	@Test
	public final void testEqualUnorderedFiles() throws IOException {
		JSONObject jsonExpected = scanJsonFile("src/test/resources/configFile_expected.json");
		JSONObject jsonGiven = scanJsonFile("src/test/resources/configFile.json");
		
		//http://stackoverflow.com/questions/2253750/compare-two-json-objects-in-java
		assertEquals("JSON found to be null [jsonExpected]", true, jsonExpected != null);
		assertEquals("JSON found to be null [jsonGiven]", true, jsonGiven != null);
		
		Assert.assertThat(jsonGiven, SameJSONAs.sameJSONObjectAs(jsonExpected).allowingAnyArrayOrdering());
	}
	
	@Test
	public final void testEqualOrderedFiles() throws IOException {
		JSONObject jsonExpected = scanJsonFile("src/test/resources/configFile.json");
		JSONObject jsonGiven = scanJsonFile("src/test/resources/configFile.json");
		
		//http://stackoverflow.com/questions/2253750/compare-two-json-objects-in-java
		assertEquals("JSON found to be null [jsonExpected]", true, jsonExpected != null);
		assertEquals("JSON found to be null [jsonGiven]", true, jsonGiven != null);
		
		Assert.assertThat(jsonGiven, SameJSONAs.sameJSONObjectAs(jsonExpected).allowingAnyArrayOrdering());
	}
	
	@Test
	public final void testUnequalOrderedFiles() throws IOException {
		JSONObject jsonExpected = scanJsonFile("src/test/resources/configFile.json");
		JSONObject jsonGiven = scanJsonFile("src/test/resources/configFile_unequal.json");
		
		//http://stackoverflow.com/questions/2253750/compare-two-json-objects-in-java
		assertEquals("JSON found to be null [jsonExpected]", true, jsonExpected != null);
		assertEquals("JSON found to be null [jsonGiven]", true, jsonGiven != null);
		
		Assert.assertThat(jsonGiven, not(SameJSONAs.sameJSONObjectAs(jsonExpected).allowingAnyArrayOrdering()));
	}
	
	/**
	 * Scans a given JSON file into a usable JSONObject
	 * @param filename [String] Name of the JSON file to scan.
	 * @return A comparable JSONObject representation of the passed in JSON file.
	 * @throws IOException 
	 */
	private JSONObject scanJsonFile(String filename) throws IOException {
		JSONObject retVal = null;
		
		// Reference: http://javarevisited.blogspot.com/2015/09/how-to-read-file-into-string-in-java-7.html
		retVal = new JSONObject(new String(Files.readAllBytes(Paths.get(filename))));
		
		return retVal;
	}
	
}
