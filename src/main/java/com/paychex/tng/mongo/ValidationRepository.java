package com.paychex.tng.mongo;

import java.util.List;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ValidationRepository extends MongoRepository<Validation, String> {

	public List<Validation> findByFormId(String formId);
	public Validation findByClientNumber(String clientNumber);

}