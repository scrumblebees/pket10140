package com.paychex.tng.mongo;

import java.util.ArrayList;
import java.util.List;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

/**
 * Representation of the 'validation' collection
 *
 */
@Document
public class Validation {
	@Id
	public String id;

	public String _class;
	public String formId;
	public String clientNumber;
	public String pdfFileName;
	public String pageNumber;
	public String pageIndex;
	
	public List<Field> fields;
	
	public Validation() {}
	
	public Validation(String formId, String clientNumber, String pdfFileName, List<Field> fields) {
		this.formId = formId;
		this.clientNumber = clientNumber;
		this.pdfFileName = pdfFileName;
		this.fields = fields;
	}
	
	public Validation(String formId, String clientNumber, String pdfFileName) {
		this(formId, clientNumber, pdfFileName, new ArrayList<Field>());
	}
	
	
	@Override
	public String toString() {
		return String.format(
				"{\"_id\":{\"$oid\":\"%s\"}, \"_class\":\"%s\", \"formID\":\"%s\", \"clientNumber\":\"%s\", \"pdfFileName\":\"%s\", \"pageNumber\":\"%s\", \"pageIndex\":\"%s\", \"fields\":%s}",
				id, _class, formId, clientNumber, pdfFileName, pageNumber, pageIndex, fields.toString());
	}
	
}
