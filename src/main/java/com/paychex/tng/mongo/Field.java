package com.paychex.tng.mongo;

import org.springframework.data.annotation.Id;

/**
 * Representation of a single field, which contains a 'name' and 'value' and is stored in a fields array.
 *
 */
public class Field {
	@Id
	public String id;
	
	public String name;
	public String value;
	
	public Field() {}
	
	public Field(String name, String value) {
		this.name = name;
		this.value = value;
	}
	
	@Override
	public String toString() {
		return String.format(
				"{\"name\":\"%s\", \"value\":\"%s\"}",
				name, value);
	}
	
}
