package com.paychex.tng.mongo;

import org.slf4j.LoggerFactory;
//import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import ch.qos.logback.classic.Logger;

@SpringBootApplication
public class Application implements CommandLineRunner {
	
	static Logger log = (Logger)LoggerFactory.getLogger(Application.class);
	
//	@Autowired
//	private ValidationRepository vRepository;
	
	public static void main(String[] args) {
		SpringApplication.run(Application.class, args);
	}
	
	@Override
	public void run(String... args) throws Exception {
		
		log.info("Running Application...");
		
		
	}
}